The acpica module includes Intel ACPI CA source code drops. No changes are made
to Intel-provided source code.

Latest version is at https://www.acpica.org/downloads/.
